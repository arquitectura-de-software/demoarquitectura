-- MySQL dump 10.11
--
-- Host: localhost    Database: drones
-- ------------------------------------------------------
-- Server version	5.0.41-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


create database drones;
use drones;
--
-- Table structure for table `dron`
--

DROP TABLE IF EXISTS `dron`;
CREATE TABLE `dron` (
  `idDron` varchar(10) NOT NULL,
  `almacenamiento` float default '0',
  `kmTotal` float default '0',
  `estado` varchar(10) NOT NULL,
  `idTipo` varchar(10) NOT NULL,
  PRIMARY KEY  (`idDron`),
  KEY `idTipo` (`idTipo`),
  CONSTRAINT `dron_ibfk_1` FOREIGN KEY (`idTipo`) REFERENCES `tipo` (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dron`
--

LOCK TABLES `dron` WRITE;
/*!40000 ALTER TABLE `dron` DISABLE KEYS */;
/*!40000 ALTER TABLE `dron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programacion`
--

DROP TABLE IF EXISTS `programacion`;
CREATE TABLE `programacion` (
  `idProgramacion` int(11) NOT NULL auto_increment,
  `idDron` varchar(10) NOT NULL,
  `idUsuario` varchar(12) NOT NULL,
  `horaInicial` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `horaFinal` timestamp NOT NULL default '0000-00-00 00:00:00',
  `fechaInicial` date default NULL,
  `fechaFinal` date default NULL,
  `destinoInicialLat` float NOT NULL,
  `destinoInicialLon` float NOT NULL,
  `destinoFinalLat` float NOT NULL,
  `destinoFinalLon` float NOT NULL,
  `peso` float NOT NULL,
  `estado` varchar(10) NOT NULL,
  PRIMARY KEY  (`idProgramacion`),
  KEY `idDron` (`idDron`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `programacion_ibfk_1` FOREIGN KEY (`idDron`) REFERENCES `dron` (`idDron`),
  CONSTRAINT `programacion_ibfk_2` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programacion`
--

LOCK TABLES `programacion` WRITE;
/*!40000 ALTER TABLE `programacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `programacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo`
--

DROP TABLE IF EXISTS `tipo`;
CREATE TABLE `tipo` (
  `idTipo` varchar(10) NOT NULL,
  `km` float default '0',
  `tempMin` float default '0',
  `tempMax` float default '0',
  `humMin` float default '0',
  `humMax` float default '0',
  `batMin` float default '0',
  `batMax` float default '0',
  `kgMax` float default '0',
  PRIMARY KEY  (`idTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo`
--

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `idUsuario` varchar(12) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY  (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-10  3:17:45
