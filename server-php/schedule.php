<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
$servername = "localhost";
$username   = "root";
$password   = "";
$dbname     = "drones";

 // Create connection
 $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

    // Add schedule
  $sql = "INSERT INTO programacion (`idProgramacion`, `idDron`, `idUsuario`, `horaInicial`, `horaFinal`, `fechaInicial`, `fechaFinal`, `destinoInicialLat`, `destinoInicialLon`, `destinoFinalLat`, `destinoFinalLon`, `peso`, `estado`) VALUES ('1', '2', '1', '0000-00-00 08:00:00', '0000-00-00 08:30:00', '2020-06-21', '2020-06-21', '2.459026', '-76.594491', '2.460291', ' -76.644668', '4', 'Programado');";
  if ($conn->query($sql) === TRUE) {
    $myJSON = json_encode("New schedule created successfully");
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }
