import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Dron } from 'src/app/models/dron/dron';
import { DronService } from '../services/dron/dron.service';

@Component({
  selector: 'app-dron',
  templateUrl: './dron.component.html',
  styleUrls: ['./dron.component.css']
})
export class DronComponent implements OnChanges {

  @Input() peso: string;

  dron: Dron;
  data=[];
  
  constructor(private dronService: DronService) { }

  ngOnChanges() {
    this.getData();
  }
 
  getData() {
    if(!this.peso) {
      return;
    }
 
    this.dronService.getDron(this.peso)
      .subscribe(data => {
        this.dron = new Dron();
        this.dron.kgMax = data.kgMax;
      });
  }

}
