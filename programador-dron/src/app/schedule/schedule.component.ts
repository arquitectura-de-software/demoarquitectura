import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  city: string;
  country: string;
  date: string;
  temperatura: number;
  peso:string;

  onSent(valuesForm: any) {
    this.city = valuesForm.city;
    this.country = valuesForm.country;
    this.date = valuesForm.date;
    this.temperatura = valuesForm.temperatura;
    this.peso = valuesForm.peso;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
