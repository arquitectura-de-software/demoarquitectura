import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { WeatherService } from 'src/app/services/weather/weather.service';
import { Weather } from 'src/app/models/weather/weather';
import { DronService } from '../services/dron/dron.service';
import { HttpClient } from '@angular/common/http';
 
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnChanges {
 
  @Input() city: string;
  @Input() country: string;
  @Input() date: string;
  @Input() temperatura: number;

  dron: DronService;
 
  weather: Weather;
 
  constructor(private weatherService: WeatherService, private http: HttpClient) { }
 
  ngOnChanges() {
    this.getData();
  }
 
  getData() {
    if(!this.city && !this.country && !this.date && !this.temperatura) {
      return;
    }
 
    this.weatherService.getWeather(this.city, this.country, this.date, this.temperatura)
      .subscribe(data => {
        this.weather = new Weather();
        this.weather.city = data.name;
        this.weather.country = data.sys.country;
        this.weather.temp = data.main.temp;
        this.weather.tempMin = data.main.temp_min;
        this.weather.tempMax = data.main.temp_max;
        let weather = data.weather[0];
        this.weather.icon = weather.icon;
        this.weather.description = weather.description;
        this.weather.temperatura = this.temperatura;
      });
  }

  public onSchedule(): void {
    this.http.get('http://localhost/drones/schedule.php').subscribe((res: Response) => {
      console.log("Vamos bien");
    }, error => console.error(error));
  }
 
}