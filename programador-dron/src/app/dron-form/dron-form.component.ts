import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dron-form',
  templateUrl: './dron-form.component.html',
  styleUrls: ['./dron-form.component.css']
})
export class DronFormComponent{

  _peso: string;
  @Output() sent = new EventEmitter<any>();

  constructor() { }

  onSend() {
    this.sent.emit({
      peso: this._peso
    });
  }

}
