import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DronFormComponent } from './dron-form.component';

describe('DronFormComponent', () => {
  let component: DronFormComponent;
  let fixture: ComponentFixture<DronFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DronFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DronFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
