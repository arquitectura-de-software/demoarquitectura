import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { WeatherFormComponent } from './weather-form/weather-form.component';
import { FormsModule }   from '@angular/forms';
import { WeatherComponent } from './weather/weather.component';
import { HomeComponent } from './home/home.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { DronComponent } from './dron/dron.component';
import { DronFormComponent } from './dron-form/dron-form.component';
import { RutaComponent } from './ruta/ruta.component';

import { GoogleMapsModule } from '@angular/google-maps';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations: [
    AppComponent,
    WeatherFormComponent,
    WeatherComponent,
    HomeComponent,
    ScheduleComponent,
    DronComponent,
    DronFormComponent,
    RutaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    GoogleMapsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAI2GKbOnx9W91KSRaNjdlAlKjqPm2dHxg',
      libraries: ['places', 'drawing', 'geometry'],
}),
NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
