import { Component, OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ruta',
  templateUrl: './ruta.component.html',
  styleUrls: ['./ruta.component.css']
})
export class RutaComponent {
  closeResult: string;
  lat: number;
  lng: number;
  data = [];
  latlng = [];
  start_end_mark = [];

  constructor(private http: HttpClient,private modalService: NgbModal) {
    this.http.get('http://localhost/drones/ruta.php').subscribe(data => {
    this.data.push(data);

    this.latlng = [
      [
        parseFloat(data[0].destinoInicialLat),
        parseFloat(data[0].destinoInicialLon)
      ],
      [
        parseFloat(data[0].destinoFinalLat),
        parseFloat(data[0].destinoFinalLon)
      ]
    ];
    let lt = this.latlng
    this.start_end_mark.push(lt[0]);
    this.start_end_mark.push(lt[lt.length - 1]);
    }, error => console.error(error));

    
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
